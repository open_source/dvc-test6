import json
from pathlib import Path
import sys

import numpy as np
import pandas as pd
from tqdm import tqdm as tqdmn
import time
import pickle
from sklearn.decomposition import PCA, FastICA, TruncatedSVD
import spacy
from gensim.models import KeyedVectors
from queue import Queue
from threading import Thread
import time

import yaml
import os

class calcText:
    def __init__(self, text, name):
        self.name = name
        self.text = text
        self.lemmas=[]
    def pipe(self, nlp):
        tokens = list(nlp.pipe([self.text]))[0]
        for token in tokens:
            if token.is_alpha and len(token.lemma_) >= 3:
                self.lemmas.append(token.lemma_)


class procWorker(Thread):

    def __init__(self, queue, nlp):
        Thread.__init__(self)
        self.queue = queue
        self.nlp = nlp

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            file = self.queue.get()
            try:
                file.pipe(self.nlp)
            finally:
                self.queue.task_done()

if __name__ == '__main__':

    path_params_parts =  os.path.abspath(__file__).split('\\')
    path_params_parts.remove(path_params_parts[len(path_params_parts)-1])
    path_dir = '\\'.join(path_params_parts)
    path_params = path_dir + "\\params.yaml"

    with open(path_params, 'r', encoding='utf8') as fd:
        params = yaml.safe_load(fd)

    method_reduction = params['method']

    path_data = Path(str(params['pin']))
    number_components = int(params['n'])
    path_to_stop_words = Path(str(params['pstop']))
    if params['p_w2v'] is not None:
        path_w2v = Path(str(params['p_w2v']))
    min_freq_words = int(params['min'])
    max_freq_words = int(params['max'])
    min_lemma_len = int(params['min_lem'])

    stop_words = []
    if len(str(path_to_stop_words)) != 0:
        stop_words = json.loads(path_to_stop_words.read_text(encoding='utf-8'))

    print('loading word2vec model')
    vectors = KeyedVectors.load(str(path_w2v))

    with open(path_data, 'rb') as f:
        data = pickle.load(f)

    print('tokenization, lemmatization')
    queue = Queue()
    # Create 8 worker threads
    for x in tqdmn(range(8)):
        nlp = spacy.load("ru_core_news_sm")
        worker = procWorker(queue, nlp)
        # Setting daemon to True will let the main thread exit even though the workers are blocking
        worker.daemon = True
        worker.start()

    calcTexts = []
    for index, value in data.iterrows():
        calc = calcText(value.text, value['name'])
        calcTexts.append(calc)
        queue.put(calc)

    with tqdmn(total=queue.unfinished_tasks) as pbar:
        last_readed_files = queue.unfinished_tasks
        while queue.unfinished_tasks != 0:
            pbar.update(last_readed_files - queue.unfinished_tasks)
            last_readed_files = queue.unfinished_tasks
            time.sleep(0.2)

    queue.join()

    all_lemmas = []
    for lemma_file_list in calcTexts:
        for lemma in lemma_file_list.lemmas:
            all_lemmas.append(lemma)

    word_freq = pd.Series(all_lemmas).value_counts()
    word_freq = list(
        word[0] for word in word_freq.to_dict().items() if word[1] > 5 and word[1] < 1000)

    word_freq_str = json.dumps(word_freq, ensure_ascii=False)
    path = Path(path_dir + '\\freq_words.json')
    path.write_text(word_freq_str, encoding='utf-8')




    print('clearing lemma list')
    lemma_dict_clear = {}
    for lemma_file_list in tqdmn(calcTexts):
        lemma_new_list = []
        for lemma in lemma_file_list.lemmas:
            if not (lemma in stop_words) and (lemma in word_freq) and not (lemma in lemma_new_list):
                lemma_new_list.append(lemma)
        lemma_file_list.lemmas = lemma_new_list
        lemma_dict_clear[lemma_file_list.name] = lemma_new_list


    df_lemms = pd.DataFrame(lemma_dict_clear.items(), columns=['name', 'lemma'])
    feat_table_full = pd.merge(data, df_lemms, on='name')

    with open(path_dir + '\\lemma_table.pickle', 'wb') as f:
        pickle.dump(feat_table_full[['name', 'lemma']], f)

    lemma_list = list(item for item in feat_table_full.lemma.to_list())
    lemma_list_str = json.dumps(lemma_list, ensure_ascii=False)
    path = Path(path_dir+'\\lemma_list.json')
    path.write_text(lemma_list_str, encoding='utf-8')
    #mlflow.log_artifact('lemma_list.json')




    vector_means = {}
    null_lemma = []
    for row in tqdmn(feat_table_full.iterrows()):
        if len(row[1]['lemma']) == 0:
            null_lemma.append(row[1]['name'])
        else:
            vector_means[row[0]] = np.mean([vectors.get_vector(val) for val in row[1]['lemma']],
                                           axis=0)
    word2vec_table = pd.DataFrame.from_dict(vector_means).transpose()

    # производим уменьшение вектора текста и сохраняем модель
    if method_reduction == 'pca':
        alg_w2v = PCA(n_components=number_components)
        alg_w2v.fit(word2vec_table)
        with open(path_dir + '\\w2v_model_reduction_pca', 'wb') as f:
            pickle.dump(alg_w2v, f)
        #mlflow.log_artifact('w2v_model_reduction_pca')

    elif method_reduction == 'svd':
        alg_w2v = TruncatedSVD(n_components=number_components)
        alg_w2v.fit(word2vec_table)
        with open(path_dir + '\\w2v_model_reduction_svd', 'wb') as f:
            pickle.dump(alg_w2v, f)
        #mlflow.log_artifact('w2v_model_reduction_svd')

    elif method_reduction == 'fastica':
        alg_w2v = FastICA(n_components=number_components)
        alg_w2v.fit(word2vec_table)
        with open(path_dir + '\\w2v_model_reduction_fastica', 'wb') as f:
            pickle.dump(alg_w2v, f)
        #mlflow.log_artifact('w2v_model_reduction_fastica')

    word2vec_table_compress = pd.DataFrame(alg_w2v.transform(word2vec_table), index=word2vec_table.index)

    word2vec_cols = [str(fea) + '_w2v' for fea in word2vec_table_compress]
    word2vec_table_compress.columns = word2vec_cols

    with open(path_dir + '\\w2v_table.pickle', 'wb') as f:
        pickle.dump(pd.concat([feat_table_full['name'], word2vec_table_compress], axis=1), f)
    print('data processed')